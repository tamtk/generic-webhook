#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin
TEMP_LOGFILE=$(mktemp /tmp/deploy-tmp.XXXXXXXXX)
LOG="/var/log/git-webhook/webhook-deploy.log"
if [ "$#" -ne 1 ]; then
    echo "[$(date +%s)][$id][$id] Deployment failed due to missing args" | tee -a $LOG;
    exit 5;
fi
id=${1:-1}
echo "[$(date +%s)][$id] Starting restart for $id at $(date '+%Y-%m-%d %H:%M:%S')"| tee -a $LOG
sudo systemctl restart nginx >& $TEMP_LOGFILE
if [ $? -ne 0 ]; then
    echo "[$(date +%s)][$id] Failed while restarting Nginx"| tee -a $LOG;
    echo "failed;Restart of $id failed at Nginx restart;$id" > ../tmp/webhook_restart_$id.status
    exit 3;
fi
# Remove tab
sed -i "s/\\t//" $TEMP_LOGFILE
# Add some space
sed -i "s/^/    /" $TEMP_LOGFILE
sed -i "s/^/\[$(date +%s)\][$id] /" $TEMP_LOGFILE
cat $TEMP_LOGFILE | tee -a  $LOG
sudo systemctl restart php-fpm >& $TEMP_LOGFILE
if [ $? -ne 0 ]; then
    echo "[$(date +%s)][$id] Failed while reloading php"| tee -a $LOG;
    echo "failed;Restart of $id failed at php-fpm restart;$id" > ../tmp/webhook_restart_$id.status
    exit 4;
fi
# Remove tab
sed -i "s/\\t//" $TEMP_LOGFILE
# Add some space
sed -i "s/^/    /" $TEMP_LOGFILE
sed -i "s/^/\[$(date +%s)\][$id] /" $TEMP_LOGFILE
cat $TEMP_LOGFILE| tee -a $LOG
rm $TEMP_LOGFILE
echo "success;Restart of $id successful;$id" > ../tmp/webhook_restart_$id.status
