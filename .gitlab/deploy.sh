#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin
TEMP_LOGFILE=$(mktemp /tmp/deploy-tmp.XXXXXXXXX)
LOG="/var/log/git-webhook/webhook-deploy.log"
if [ "$#" -ne 2 ]; then
    echo "[$(date +%s)][$id] Deployment failed due to missing args" | tee -a $LOG;
    exit 3;
fi
id=${2:-1}
echo "[$(date +%s)][$id] Starting deployment of $1 at $(date '+%Y-%m-%d %H:%M:%S')"|tee -a $LOG
git checkout $1 >& $TEMP_LOGFILE
if [ $? -ne 0 ] ; then echo "[$(date +%s)][$id] Deployment of $1 failed at checkout"|tee -a $LOG; exit 1; fi
# Remove tab
sed -i "s/\\t//" $TEMP_LOGFILE
# Add some space
sed -i "s/^/    /" $TEMP_LOGFILE
sed -i "s/^/\[$(date +%s)\][$id] /" $TEMP_LOGFILE
cat $TEMP_LOGFILE | tee -a $LOG
git pull origin $1 >& $TEMP_LOGFILE
if [ $? -ne 0 ] ; then echo "[$(date +%s)][$id] Deployment of $1 failed at pull" | tee -a $LOG; exit 2; fi
# Remove tab
sed -i "s/\\t//" $TEMP_LOGFILE
# Add some space
sed -i "s/^/    /" $TEMP_LOGFILE
sed -i "s/^/\[$(date +%s)\][$id] /" $TEMP_LOGFILE
cat $TEMP_LOGFILE | tee -a $LOG
rm $TEMP_LOGFILE

