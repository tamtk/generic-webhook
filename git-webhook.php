<?php
function exit_webhook($message,$code, $output = array()){
    $return = array('message' => $message, "output" => $output,'code' => $code);
    http_response_code($return['code']);
    die(json_encode($return));
}

$config = parse_ini_file('config.webhook.php');

/* security */
$access_token   = $config['deploy_token'];
if(!isset($access_token)) {
    exit_webhook("deploy_token not found in config",400);
}
$access_ip      = $config['authorized_ip'];
if(!isset($access_ip)) {
    exit_webhook("authorized_ip not found in config",400);
}
$log_file       = $config['log_file'];
if(!isset($log_file)) {
    exit_webhook("log_file not found in config",400);
}
$token_header   = $config['token_header'];
if(!isset($token_header)) {
    exit_webhook("token_header not found in config",400);
}

/* get user token and ip address */
$client_token = $_SERVER[$token_header];
$client_ip = $_SERVER['REMOTE_ADDR'];
$client_event = $_SERVER['HTTP_X_GITLAB_EVENT'];
$id = $_SERVER['HTTP_X_GITLAB_SHA'];

if(!is_file($log_file)){
    $contents = 'Creating log file';
    file_put_contents($log_file, $contents);
}
/* Test if writable */
if (!is_writable($log_file)) {
    exit_webhook('Cannot write to log '.$log_file,400);
}
/* test token */
if ($client_token !== $access_token){
    fwrite($fs, '['.time().']['.$id.'] Received invalid token ['.$client_token.']'.PHP_EOL);
    $fs and fclose($fs);
    exit_webhook("Access forbidden, received invalid token [$client_token]",403);
}
/* test event */
if ($client_event !== 'Push Hook'){ # Legacy
    fwrite($fs, '['.time().']['.$id.'] Event ['.$client_event.'] received'.PHP_EOL);
    $fs and fclose($fs);
    exit_webhook("Event [$client_event] received, not a 'Push Hook' event, exiting",200);
}


/* get json data */
$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!isset($data["ref"])){
    fwrite($fs, '['.time().']['.$id.'] Missing branch ref'.PHP_EOL);
    $fs and fclose($fs);
    exit_webhook("Missing branch ref",400);
}
if(!isset($id)){
    fwrite($fs, '['.time().']['.$id.'] Missing header SHA'.PHP_EOL);
    $fs and fclose($fs);
    exit_webhook("Missing header SHA",400);
}

/* get branch */
$branch = $data["ref"];
$branch_exploded = explode('/',$branch);
$short_branch = end($branch_exploded);

if(!isset($data["function"])){
    fwrite($fs, '['.time().']['.$id.'] Missing function'.PHP_EOL);
    $fs and fclose($fs);
    exit_webhook("Missing function",400);
}

fwrite($fs, '['.time().']['.$id.'] '.ucfirst($data["function"]).' '.print_r($short_branch, true).PHP_EOL);

if(isset($data["function"]) && $data["function"] === "deploy") {
    # Start of deploy
    exec("bash .gitlab/deploy.sh ".escapeshellarg($short_branch)." ".escapeshellarg($id)." 2>&1",$output, $return_code);
    #echo "Function Deploy";
} elseif(isset($data["function"]) && $data["function"] === "restart") {
    # Start of restart
    # We want to return response before the restart process to have return 0
    session_write_close();      //close the session
    fastcgi_finish_request();   //this returns 200 to gitlab, and processing continues
    exec("bash .gitlab/restart.sh ".escapeshellarg($id)." &",$output, $return_code);
} elseif(isset($data["function"]) && $data["function"] === "status") {
    # Start of Status
    $restart_file=".tmp/webhook_restart_${id}.status";
    fwrite($fs, '['.time().']['.$id.'] Getting content of '.$restart_file.PHP_EOL);
    $status = @file_get_contents($restart_file);
    $output = explode(';',$status);
    @unlink($restart_file);
    $fs and fclose($fs);
    exit_webhook("got status of restart",200,array("status" => "$output[0]","message" => "$output[1]", "id" => "$output[2]"));
}
# Test return
if($return_code){
    fwrite($fs, '['.time().']['.$id.'] Deployment failed with return code '.$return_code.PHP_EOL);
    $fs and fclose($fs);
    exit_webhook("Deployment failed with return code $return_code",400,$output);
} else {
    fwrite($fs, '['.time().']['.$id.'] Deployment successful with return code '.$return_code.PHP_EOL);
    $fs and fclose($fs);
    exit_webhook("Deployment successful with return code $return_code",200,$output);
}
?>
