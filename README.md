# tk.ninh - generic webhook
----------------------------------------
[![major](https://badgen.net/badge/version/0.2/green)]()
[![stars](https://badgen.net/badge/stars/★★★★/green)]()
[![language](https://badgen.net/badge/language/php|bash/green)]()
[![framework](https://badgen.net/badge/framework/gitlab-cicd|yaml/green)]()
## Introduction

This project is make **Webhook API** base on php language. I used php create webhook to listen trigger from Gitlab CD. Then, it do the job base on internal script defind for each action.

The pipeline just executed when you adding new TAG on **master** branch

The project include information bellow:

* Create simple webhook
* Create simple CI/CD to integration your project

## Pre-requisite

* Setup gitlab runners for your project ([How to install gitlab runners](https://docs.gitlab.com/runner/install/)). if you use gitlab.com, you can use share runner public.
* 


## How to use this project

## 1. Clone project to your workspace

```bash
git clone https://gitlab.com/tamtk/generic-webhook.git
```

* Go to generic-webhook directory.
```bash
cd generic-webhook
```

* Copy file follow:

1. **.gitlab-ci.yml**: Root directory of your git project
2. **.gitlab/ folder and git-webhook.php**: Root directory of your source code( same address with index.*)


Now, You have done for adding webhook on your source code.

## 2. Modify your gitlab CI/CD.

Currentlly, I make 3 state on gitlab CI/CD
* deploy: Deploy your source web to server running webserver
* restart: Restart your webservice
* status: Check status of webservice after restart

if you want to more state on gitlab CI/CD like verify systax, check code function... feel free to create issue for me.

### Which parameters need modify?

* Open gitlab-ci.yml
```bash
vim .gitlab-ci.yml
```

* TAG-RUNNER-YOU-CHOOSE: chage it to TAG of runner your want.
* IP OF WEBSERVER 01: ip web server 1
* IP OF WEBSERVER 02: ip web server 2

## 3. Modify your operation behind Webhook

* Go to folder **.gitlab/**
* Open restart.sh

```bash
vim restart.sh
```
* Change your webservice at line 12 and 24, if you're using nginx+php-fpm, no need this action.